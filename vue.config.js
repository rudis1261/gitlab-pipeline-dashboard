module.exports = {
  transpileDependencies: [
    /\bvue-awesome\b/
  ],
  publicPath: process.env.PUBLIC_PATH || '/'
}